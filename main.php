<?php
     session_start();
     require_once 'plugins/functions.class.php';
     $imgWidth = 400;
     $imgHeight = 250;
     $fontSize = 12;
     $fontNormal = 'plugins/fonts/Lato-Regular.ttf';
     $fontBold = 'plugins/fonts/Lato-Bold.ttf';
     define('DIR_DOWNLOAD', 'download/');

     $formData = $_POST;
     $error = array();
     $response = array();

     $myDesign = new functionClass();

     if ($myDesign->validateForm($formData, $error)) {
         //create new image
         $imageSource = $myDesign->createSourceImage($imgWidth, $imgHeight);
         $fontColor = imagecolorallocate($imageSource, 0, 0, 0); //color font

         //insert Fullname to card
         $fullname = $formData['first_name'] .' '.$formData['last_name'];
         $myDesign->insertFullName($imageSource,$fontSize, $fontColor, $fontNormal, $fullname);

         //insert company to card
         $companyText = $formData['company_name'];
         $myDesign->insertCompany($imageSource, $fontSize, $fontBold, $fontColor, strtoupper($companyText));

         //insert email to card
         $emailAddress = $formData['email_address'];
         $myDesign->insertEmail($imageSource, $imgHeight, $fontSize, $fontColor, $fontNormal, $emailAddress);

         //insert phone to card
         $phoneNumber = $formData['phone_number'];
         $myDesign->insertPhoneNumber($imageSource, $fontSize, $fontNormal, $fontColor, $phoneNumber);

         //delete img old
         if (isset($_SESSION['nameimg']) && is_file(DIR_DOWNLOAD . $_SESSION['nameimg'].'.png')) {
             unlink(DIR_DOWNLOAD.$_SESSION['nameimg'].'.png');
         }
         $_SESSION['nameimg'] = time();

         header("Content-Type: image/png");
         imagepng($imageSource, DIR_DOWNLOAD .$_SESSION['nameimg'].'.png');
         imagedestroy($imageSource);

         $response = array(
             'status' => true,
             'src' => DIR_DOWNLOAD .$_SESSION['nameimg'].'.png'
         );
     } else {
         $response = array(
             'status' => false,
             'error' => $error
         );
     }

    echo json_encode($response);
?>